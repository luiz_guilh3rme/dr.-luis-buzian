<div class="mobile-cta">
	<a href="wpp" target="_BLANK" class="whatsapp-mobile-link" aria-label="Fale conosco no Whatsapp" title="Fale conosco no Whatsapp">
		<i class="fab fa-whatsapp"></i>
	</a>
	<a href="tel:1147502446" class="call-link footer-btn">
		LIGAR
	</a>
	<button class="open-call-cta footer-btn">
		ENTRE EM CONTATO
	</button>
</div>