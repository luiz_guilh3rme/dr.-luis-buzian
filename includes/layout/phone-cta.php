<div class="cta-overlay">
	<button class="close-modal close-cta-forms" aria-label="Fechar Modal">
		&times;
	</button>
	<div class="form-wrapper-all">
		<div class="form-picker">
			<button class="form-pickers" data-instance="01">
				<i class="fa fa-phone"></i>
				ME LIGUE AGORA
			</button>
			<button class="form-pickers active" data-instance="02">
				<i class="fa fa-clock alt"></i>
				ME LIGUE DEPOIS
			</button>
			<button class="form-pickers" data-instance="03">
				<i class="fa fa-comments alt"></i>
				DEIXE UMA MENSAGEM
			</button>
		</div>
		<div class="instance" data-instance="01">
			<div class="leave-message">
				<legend class="leave-title">
					<span class="variant">
						NÓS TE LIGAMOS!
					</span>
					Informe seu telefone que entraremos em
					contato o mais rápido possível.
				</legend>
				<div class="fields cleared">
					<?php echo do_shortcode('[contact-form-7 id="30" title="Nós te Ligamos"]'); ?>
					<p class="callers">Você já é a <span class="number">5</span> pessoa a solicitar uma ligação.</p>
				</div>
			</div>
		</div>
		<div class="instance active" data-instance="02">
			<div class="leave-message schedule-time">
				<legend class="leave-title">
					Gostaria de agendar e receber uma
					chamada em outro horário?
				</legend>
				<div class="fields cleared">
					<?php echo do_shortcode('[contact-form-7 id="31" title="Me Ligue Depois"]'); ?>
					<p class="callers">Você já é a <span class="number">5</span> pessoa a solicitar uma ligação.</p>
				</div>
			</div>
		</div>
		<div class="instance" data-instance="03">
			<div class="leave-message">
				<legend class="leave-title">
					Deixe sua mensagem! Entraremos em
					contato o mais rápido possível.
				</legend>
				<div class="fields cleared">
					<?php echo do_shortcode('[contact-form-7 id="32" title="Deixe uma Mensagem"]'); ?>
					<p class="callers">Você já é a <span class="number">3</span> pessoa a deixar uma mensagem.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="call-cta-wrapper">
	<div class="cta-tooltip">
		<p class="tooltip-text">
			Gostaria de agendar por telefone?
		</p>
		<button class="confirm">SIM</button>
		<button class="close-cta" aria-label="Fechar CTA de whatsapp">
			&times;
		</button>
	</div>
	<button class="phone-icon">
		<i class="fa fa-phone"></i>
	</button>
</div>