<div class="upper-partition">
	<div class="center-content bigger flexed row spaced centered" style="width: 1190px;">
		<div class="group dibfather" style="font-size: 15px;">
			<span class="t-gray">
				<i class="fa fa-map-marker-alt t-purple"></i>
				<b>São Paulo: Rua Cubatão 408, 8º andar - Paraíso</b> | São Paulo/SP
			</span>
			<span class="t-gray">
				<i class="fa fa-phone t-purple" style="color: #979e23;"></i>
				<b>
					<a href="tel:1147502446" target="_BLANK" class="phone-number-upper">(11) 4750-2446</a>
				</b>
			</span>
			<span class="t-gray">
				<i class="far fa-envelope t-purple"></i>
				<a class="email-upper" href="mailto:contato@drluisbuzian.com.br">atendimento@drluisbuzian.com.br</a>
			</span>
		</div>
		<!-- <div class="group dibfather">
			<a href="" class="header-social">
				<i class="fab fa-facebook"></i>
			</a>
			<a href="" class="header-social">
				<i class="fab fa-instagram"></i>
			</a>
		</div> -->
	</div>
</div>