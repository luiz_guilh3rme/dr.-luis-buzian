<nav class="header-navigation">
	<div class="center-content">
		<a href="<?php echo site_url('/'); ?>" class="mobile-logo">
			<img src="<?php bloginfo('template_url'); ?>/images/common/logo-mobile.png" alt="Logotipo Dr. Luis Buzian">
		</a>
		<ul class="main-menu flexed row spaced start">
			<li><a href="<?php echo site_url('/'); ?>">Home</a></li>
			<li><a class="is-anchor" data-anchor="#about" data-offset="200">Sobre</a></li>
			<li><a class="is-anchor" data-anchor="#differentials" data-offset="-400">Diferenciais</a></li>
			<li class="has-logo" style="top: -10px">
				<a href="<?php echo site_url('/'); ?>" class="header-logo-wrapper">
					<img src="<?php bloginfo('template_url'); ?>/images/common/logo.png" alt="Logotipo Dr. Luis Buzian">
				</a>
			</li>
			<li><a class="is-anchor" data-anchor="#anxiety">Ansiedade</a></li>
			<li><a class="is-anchor" data-anchor="#treatments">Tratamentos</a></li>
			<li><a class="is-anchor" data-anchor="#scheduling" data-offset="0">Agende</a></li>
		</ul>
		<button class="hamburger hamburger--3dx" type="button">
			<span class="hamburger-box">
				<span class="hamburger-inner"></span>
			</span>
		</button>
	</div>
</nav>