// unique scripts go here.

var header = {};

header.init = function() {
	// on scroll event
	window.onscroll = header.becomeFixed;

	// if window is loaded with offset
	header.becomeFixed(); 

	// mobile menu
	$('.hamburger').on('click', header.openMenu);
}

header.becomeFixed = function() {
	var node = document.querySelector('.header-navigation');

	if (window.pageYOffset > 50) {
		node.classList.add('fixed');
	} else {
		node.classList.remove('fixed');
	}
}

header.openMenu = function() {
	$(this).toggleClass('is-active'); 
	$('.header-navigation').toggleClass('is-open');
}

function instanceCarousels() {

	if ( helpers.isMobile ) {
		$('.becomes-carousel-on-mobile').slick({
			slidesToShow: 1,
			arrows: false,	
			adaptiveHeight: true,		
			autoplay: true, 
			autoplaySpeed: 4000,
		});
	}

}

function anchors(event) {
	// prevent default click behavior on <a>;
	event.preventDefault();

	// gather need data such as where to scroll and possible offset if set. 
	var target = $(this).data('anchor'), 
	offset = $(this).data('offset') != undefined ? $(this).data('offset') : -200;

	// animate html and body for a smooth scrolling
	$('html,body').animate({
		scrollTop: $(target).offset().top + offset
	}, 500);
}


function maskInstancing() {
    var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function (val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

    $('.phone-field').mask(SPMaskBehavior, spOptions);
    $('.date-field').mask('00/00/0000');
    $('.hour-field').mask('00:00');
}
