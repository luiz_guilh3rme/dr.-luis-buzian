
function init() {
	header.init();    
	maskInstancing();
	instanceCarousels();

	$('.is-anchor').on('click', anchors);
	

	// phone cta
	openPhoneCta();
	closeModalCta();
	printNumbers();
	$('.form-pickers').on('click', formPickers);
	$('.close-cta').on('click', closeCTA); 
	$('.phone-icon').on('click', reopenCTA);

}

// window load binds 
window.onload = init;

function DOMLoaded() {
    // these are not always necessary but sometimes they fuck with ya
	if (helpers.iOS) {
		document.querySelector('html').classList.add('ios');
	} else if (helpers.IE()) {
		document.querySelector('html').classList.add('ie');
	}
}

// domcontent binds 
document.addEventListener('DOMContentLoaded', DOMLoaded);