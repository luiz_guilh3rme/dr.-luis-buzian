<?php 
// Template name: Thank You
get_header(); 
if (have_posts()) : while(have_posts()) : the_post();
	?>
	<main class="structure">
		<section class="thanks">
			<div class="center-content cms-wrap">
				<?php the_content(); ?>
			</div>
		</section>
	</main>
	<?php 
endwhile; 
endif;
get_footer(); 
?>