<footer class="purple-bg purple-swag has-border-before">
	<div class="center-content">
		<a href="<?php echo site_url('/'); ?>" class="footer-logo-wrapper">
			<img src="<?php bloginfo('template_url'); ?>/images/common/logo-white.png" alt="Logotipo Dr. Luis Buzian" title="Logotipo Dr. Luis Buzian" class="footer-logo">
		</a>
		<p class="limited t-r t-white lig lh-xg t-centered">
			Título em Psiquiatria pela ABP (Associação Brasileira de Psiquiatria) e AMB (Associação Médica Brasileira). 
			Registro de Qualificação de Especialista pelo CFM-CRM (Conselhos Federal e Regional de Medicina).
		</p>
		<p class="footer-tagline gradient-text">
			Agende sua primeira Consulta!
		</p>
	</div>
	<div class="center-content bigger">
		<div class="locations dibfather t-centered becomes-carousel-on-mobile">
			<div class="gradient-border location-wrapper to-right dib">
				<div class="location flexed row spaced centered">
					<div class="frame-holder"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.7969116257536!2d-46.64615158502188!3d-23.57573638467567!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5990d62d047b%3A0x93c3d19fd1f04517!2sR.+Cubat%C3%A3o%2C+408+-+Para%C3%ADso%2C+S%C3%A3o+Paulo+-+SP%2C+04013-001!5e0!3m2!1spt-BR!2sbr!4v1554566612034!5m2!1spt-BR!2sbr" width="213" height="206" frameborder="0" style="border:0" allowfullscreen></iframe></div>
					<div class="info-holder">
						<h3 class="unit-title t-30 t-white lig margin-xs">
							Unidade <strong>Paraíso</strong>
						</h3>
						<div class="dibfather">
							<i class="fa fa-map-marker-alt t-g t-beige"></i>
							<p class="t-white margin-xs dib unit-info">
								<strong class="t-g">
									Rua Cubatão, nº. 408, 8º andar 
								</strong>
								<br>
								<small class="t-s">
									Paraíso | São Paulo/SP
								</small>
							</p>
							<p class="t-g t-white-bold unit-phones">
								<a href="tel:+551147502446"><i class="fa fa-phone t-beige"></i>11 4750-2446</a>
								<a href="wpp"><i class="fab fa-whatsapp t-beige"></i>11 98149.1519</a>
							</p>
						</div><!-- dibfather -->
					</div><!-- info-holder -->
				</div><!-- location -->
			</div><!-- gradient-border -->
			
		</div>
	</div>
</footer>
<div class="copyright has-border-before">
	<div class="center-content flexed row spaced centered">
		<p class="t-xs t-purple">
			Dr. Luis Buzian | Copyright © Todos os direitos Reservados. 
		</p>
		<div class="signature">
			<a href="https://www.3xceler.com.br/criacao-de-sites" rel="noopener" target="_BLANK" title="Criação de Sites" class="signature-link">
				Criação de Sites
			</a>
			<span class="sr-only">3xceler</span>
			<img src="<?php bloginfo('template_url'); ?>/images/common/signature.png" alt="Logotipo Agência 3xceler" title="Logotipo Agência 3xceler" class="signature-image">
		</div>
	</div>
</div>
<a href="wpp" class="whatsapp-cta" title="Fale conosco no Whatsapp!" target="_BLANK">
	<img src="<?php bloginfo('template_url'); ?>/images/common/whatsapp.png" alt="Logotipo Whatsapp">
</a>
<?php 
get_template_part('includes/components/mobile-cta');
get_template_part('includes/layout/phone-cta');
get_template_part('includes/components/oldie'); 
wp_footer();
?>
</body>
</html> 

