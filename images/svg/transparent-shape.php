<svg 
 xmlns="http://www.w3.org/2000/svg"
 xmlns:xlink="http://www.w3.org/1999/xlink"
 width="399px" height="452px">
<defs>
<filter filterUnits="userSpaceOnUse" id="Filter_0" x="-1px" y="-1px" width="400px" height="453px"  >
    <feOffset in="SourceAlpha" dx="6" dy="10.392" />
    <feGaussianBlur result="blurOut" stdDeviation="0" />
    <feFlood flood-color="rgb(0, 0, 0)" result="floodOut" />
    <feComposite operator="atop" in="floodOut" in2="blurOut" />
    <feComponentTransfer><feFuncA type="linear" slope="0.15"/></feComponentTransfer>
    <feMerge>
    <feMergeNode/>
    <feMergeNode in="SourceGraphic"/>
  </feMerge>
</filter>

</defs>
<g filter="url(#Filter_0)">
<path fill-rule="evenodd"  stroke-width="2px" stroke-linecap="butt" stroke-linejoin="miter" fill-opacity="0.2" opacity="0.502" fill="rgb(255, 255, 255)"
 d="M391.000,390.000 C391.000,417.614 368.614,440.000 341.000,440.000 L1.000,440.000 L1.000,80.000 L80.578,80.000 C98.748,33.746 143.799,1.000 196.500,1.000 C249.201,1.000 294.252,33.746 312.422,80.000 L391.000,80.000 L391.000,390.000 Z"/>
</g>
</svg>