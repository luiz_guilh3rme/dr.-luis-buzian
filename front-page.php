<?php get_header(); ?>

<main class="structure">
	<section class="opening-banner" id="about">
		<div class="center-content">
			<div class="group">
				<img src="<?php bloginfo('template_url'); ?>/images/props/doc-mobile.png" class="doctor-mobile-prop" alt="Doutor Sorrindo olhando o usuário">
				<h1 class="section-title med has-low-border teal-border margin-g">
					<span class="t-gray lig">
						Psiquiatra
					</span>
					<br>
					<span class="variant t-purple bold">
						Dr. Luis G. Buzian
					</span> 
				</h1>
				<p class="t-g t-gray hiline lig margin-xs" style="top: -47px;
				position: relative;"><strong>CRM:</strong>125.121 / <strong>RQE:</strong> 71.974</p>
				<p class="t-g t-gray hiline lig margin-xs" style="font-weight: 300;
				position: relative;
				top: -40px;">
				Formado  na Faculdade de Medicina do ABC, com Título em Psiquiatria pela ABP (Associação Brasileira de Psiquiatria) e
				<br>
				AMB (Associação Médica Brasileira). 
			</p>
			<p class="t-g t-gray hiline lig margin-s" style="font-weight: 300;
			position: relative;
			top: -40px;">
			Registro de Qualificação de Especialista pelo CFM-CRM <br>
			(Conselhos Federal e Regional de Medicina).
		</p>
		<img src="<?php bloginfo('template_url'); ?>/images/common/abp.jpg" alt="Logotipo Associação brasileira de psiquiatria - conceitual em verde, azul e amarelo" title="Associação brasileira de psiquiatria" class="dib opening-logo">
		<img src="<?php bloginfo('template_url'); ?>/images/common/amb.jpg" alt="Logotipo AMB - Associação médica brasileira" class="dib opening-logo">
	</div>
	<img src="<?php bloginfo('template_url'); ?>/images/props/doc.png" alt="Doutor Sorrindo olhando o usuário" class="doctor-persona">
</div>
</section>
<section class="purple-bg content-cards has-border-before" id="differentials">
	<div class="uncontained-background"></div>
	<div class="center-content card-wrapper flexed row spaced start becomes-carousel-on-mobile">
		<div class="card-outer one-third">
			<div class="card-inner">
				<img class="card-icon" src="<?php bloginfo('template_url'); ?>/images/icons/01.png" alt="">
				<h2 class="card-title">
					Primeira Consulta <br> Extendida
				</h2>
				<p class="t-g t-gray lig lh-xxg card-description">
					Primeira consulta extendida com 2 horas de duração. Título de Psiquiatria pela ABP.
				</p>
			</div>
		</div>
		<div class="card-outer one-third">
			<div class="card-inner">
				<img class="card-icon" src="<?php bloginfo('template_url'); ?>/images/icons/02.png" alt="">
				<h2 class="card-title">
					Conforto e <br> Tranquilidade
				</h2>
				<p class="t-g t-gray lig lh-xxg card-description">
					Tecnologia, infraestrutura e conforto necessários. Além de possuir localização privilegiada, perto do metrô Paraíso.
				</div>
			</div>
			<div class="card-outer one-third">
				<div class="card-inner">
					<img class="card-icon" src="<?php bloginfo('template_url'); ?>/images/icons/03.png" alt="">
					<h2 class="card-title">
						Profissional <br> Qualificado
					</h2>
					<p class="t-g t-gray lig lh-xxg card-description">
						Formado na Faculdade de Medicina do ABC, título em Psiquiatria pela ABP (Associação Brasileira de Psiquiatria) e 
						AMB (Associação Médica Brasileira).
					</p>
				</div>
			</div>
		</div>
		<p class="mobile-helper-text t-xs t-white t-centered"><i class="fa fa-angle-double-left"></i> Deslize para ver mais. <i class="fa fa-angle-double-right"></i></p>
	</section>
	<section class="purple-bg main-treatment" id="anxiety">
		<div class="center-content flexed row spaced start">
			<div class="group">
				<h2 class="section-title huge gradient-text margin-xxg">
					<span class="t-white">
						Tratamento para Transtorno
					</span>
					<br>
					<span class="variant">de Ansiedade</span>
				</h2>	
				<p class="t-g lig t-white hiline margin-s">
					A ansiedade é uma reação normal diante de situações que podem provocar medo, dúvida ou expectativa. É considerada normal a ansiedade que se manifesta nas horas que antecedem uma entrevista de emprego, a publicação dos aprovados num concurso, o nascimento de um filho, uma viagem a um país exótico, uma cirurgia delicada, ou um revés econômico. Nesses casos, a ansiedade funciona como um sinal que prepara a pessoa para enfrentar o desafio e, mesmo que ele não seja superado,  favorece sua adaptação às novas condições de vida.
				</p>
				
				<p class="t-g lig t-white hiline">	
					Os sintomas podem variar de uma pessoa para outra. Além de inquietação, fadiga, irritabilidade, dificuldade de concentração, tensão muscular, existem outras queixas que podem estar associadas ao transtorno da ansiedade generalizada: palpitações, falta de ar, taquicardia, aumento da pressão arterial, sudorese excessiva, dor de cabeça, alteração nos hábitos intestinais, náuseas, aperto no peito, dores musculares. 
				</p></br></br></br>
				
				<p class="t-g lig t-white hiline">
					O diagnóstico do TAG leva em conta a história de vida do paciente, a avaliação clínica criteriosa e, quando necessário, a realização de alguns exames complementares.

					Como os sintomas podem ser comuns a várias condições clinicas diferentes que exigem tratamento específico, é fundamental estabelecer o diagnóstico diferencial com TOC, síndrome do pânico ou fobia social, por exemplo.
				</p>
			</div>
			<div class="group">
				<div class="bordered-under gradient-border to-bottom bottom-right-radius" aria-hidden="true"></div>
				<div class="gradient-border to-bottom pic-holder bottom-right-radius">
					<img src="<?php bloginfo('template_url'); ?>/images/props/woman.jpg" alt="Mulher com expressão apreensiva" class="picture-bordered">
				</div>
				<div class="gradient-border to-right wrap-gradient">
					<button class="wrapped-gradient">AGENDAR CONSULTA</button>
				</div>
			</div>
		</div>
	</section>
	<section class="purple-bg other-treatments" id="treatments">
		<div class="center-content">
			<h2 class="section-title huge gradient-text margin-xxg">
				<span class="t-white">
					A importância da SAÚDE
				</span>
				<br>
				<span class="variant">MENTAL em sua Vida</span>
			</h2>	
			<p class="t-g lig t-white hiline margin">	
				A saúde mental é o conjunto de nosso bem-estar cognitivo e emocional, psicológico e social. Qualquer alteração na saúde mental afeta a forma como sentimos, pensamos, agimos, lidamos com o estresse e com os relacionamentos pessoais e até mesmo a nossa capacidade de escolha. Por isso, a saúde mental é importante em todas as fases da vida, desde a infância até a idade adulta. 
			</p>
			<div class="treatments dibfather becomes-carousel-on-mobile">
				<?php 
				$args = array(
					'post_type' => 'tratamentos', 
					'posts_per_page' => -1, 
				);
				$treatments = new WP_Query($args);
				if ($treatments->have_posts()) : 
					while ($treatments->have_posts()) : 
						$treatments->the_post(); 
						?>
						<div class="treatment-block gradient-border to-bottom dib">
							<div class="treatment-block-inner">
								<div class="gradient-border to-bottom treatment-picture-wrapper">
									<?php
								// wrapInPre(get_the_post_thumbnail_url() , false);  
									if ( get_the_post_thumbnail() ) {
										the_post_thumbnail('full', array('class' => 'treatment-picture'));
									} else {
										'<img src="http://placehold.it/300x300" alt="Imagem base 300x300" class="treatment-picture">'; 
									}
									?>
								</div>
								<h3 class="treatment-title">
									<?php the_field('title_first_line'); ?>
									<span>
										<?php the_field('title_second_line'); ?>
									</span>
								</h3>
								<p class="treatment-description t-xs t-white lig lh-s margin">
									<?php echo strip_tags(get_the_content()); ?>
								</p>
								<div class="gradient-border to-right treatment-btn-wrapper">
									<button class="form-anchor treatment-btn is-anchor" data-anchor="#scheduling" data-offset="0">AGENDAR CONSULTA</button>
								</div>
							</div>
						</div>
						<?php 
					endwhile; 
				endif; 
				wp_reset_postdata();
				?>
			</div>
			<p class="mobile-helper-text t-xs t-white t-centered"><i class="fa fa-angle-double-left"></i> Deslize para ver mais. <i class="fa fa-angle-double-right"></i></p>
		</div>
	</section>
	<section class="pre-schedule" id="scheduling">
		<div class="center-content">
			<h2 class="section-title huge t-purple margin-xxg has-low-border teal-border margin-xg">
				Pré-Agende sua
				<br>
				<span class="variant bold">
					Consulta
				</span>
			</h2>	
			<div class="form-wrapper">
				<p class="t-g t-gray lig hiline margin-s">
					Consultas com base na psiquiatria psicodinâmica, <strong>não vinculada a convênios médicos</strong>. 
					Se você precisa de mais informações, tem dúvidas, ou gostaria de agendar uma consulta, entre em contato. Você pode agendar por telefone, e-mail ou pelo formulário abaixo. 
				</p>
				<h2 style="font-size: 40px; color: #3b3a61;     font-weight: 700;">Agende: <i class="fa fa-phone t-beige"></i> (11) 4750.2446 </h2></br></br></br>
				<div class="form-body contact-main-form flexed row spaced centered">
					<?php echo do_shortcode('[contact-form-7 id="5" title="Contato Básico"]'); ?>
				</div>
			</div>
			<img class="woman-prop-form" src="<?php bloginfo('template_url'); ?>/images/props/woman.png" alt="Mulher sorrindo olhando em direção ao usuário" title="Mulher sorrindo olhando em direção ao usuário">
		</div>
	</section>
</main>
<?php get_footer(); ?>