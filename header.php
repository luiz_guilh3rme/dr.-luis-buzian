<!DOCTYPE html>
<!--[if IE]><html lang="pt-br" class="lt-ie9 lt-ie8"><![endif]-->
<html lang="pt-br">
<?php get_template_part('includes/components/head'); ?>
<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5GRSW3H"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<header class="centered-logo">

		<?php 
		get_template_part('includes/layout/header/header-upper-partition'); 
		get_template_part('includes/layout/header/header-navigation');
		?>
		
	</header>